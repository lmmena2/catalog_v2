from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=200)
    sku = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    brand = models.CharField(max_length=200)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='adder')
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='updater')

    def __str__(self):
        return self.name


class ProductQueryTrack(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='query_product')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='query_user')
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.product.name
