from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer

from product.models import Product, ProductQueryTrack


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'name', 'sku', 'price', 'brand']


class TrackerUserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username', 'email']


class TrackerSerializer(ModelSerializer):
    product = ProductSerializer(many=False)
    user = TrackerUserSerializer(many=False)

    class Meta:
        model = ProductQueryTrack
        fields = ['id', 'product', 'user', 'timestamp']
