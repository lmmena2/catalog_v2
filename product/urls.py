from django.urls import path
from rest_framework.routers import DefaultRouter

from product.views import ProductAPIView, TrackAPIView, TrackByProductAPIView

router = DefaultRouter()
router.register('product', ProductAPIView, basename='product')
router.register('track', TrackAPIView, basename='track')
app_name = 'product'
urlpatterns = [
                  path('track/product/<id>/', TrackByProductAPIView.as_view())
              ] + router.urls
