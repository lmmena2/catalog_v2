from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect

# Create your views here.
from django.template.loader import get_template
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from immena2 import settings
from product.models import Product, ProductQueryTrack
from product.serializer import ProductSerializer, TrackerSerializer


class ProductAPIView(viewsets.GenericViewSet,
                     mixins.CreateModelMixin,
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

    def create(self, request, *args, **kwargs):
        if self.request.user.is_staff or self.request.user.is_admin:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.validated_data['added_by'] = self.request.user
            serializer.save()
            return Response(serializer.data, status.HTTP_201_CREATED)
        else:
            return Response({'detail': 'You are not authorized to add product'}, status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        tr = ProductQueryTrack.objects.create(product=obj, user=self.request.user)
        tr.save()
        return Response(self.serializer_class(obj, many=False).data, status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        if self.request.user.is_staff or self.request.user.is_admin:
            self.get_object().delete()
            return Response({'detail': 'Product is deleted'}, status.HTTP_204_NO_CONTENT)
        else:
            return Response({'detail': 'You are not authorized to delete product'}, status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        if self.request.user.is_staff or self.request.user.is_admin:
            obj = self.get_object()
            serializer = self.serializer_class(instance=obj, data=request.data)
            serializer.is_valid(raise_exception=True)
            if obj == serializer.validated_data:
                serializer.save()
                return Response(serializer.data)
            pts = []
            if obj.name != serializer.validated_data['name']:
                pts.append({'info': 'name', 'old': obj.name, 'new': serializer.validated_data['name']})
            if obj.sku != serializer.validated_data['sku']:
                pts.append({'info': 'sku', 'old': obj.sku, 'new': serializer.validated_data['sku']})
            if obj.price != serializer.validated_data['price']:
                pts.append({'info': 'price', 'old': obj.price, 'new': serializer.validated_data['price']})
            if obj.brand != serializer.validated_data['brand']:
                pts.append({'info': 'brand', 'old': obj.brand, 'new': serializer.validated_data['brand']})
            serializer.validated_data['updated_by'] = self.request.user
            serializer.save()
            mail_subject = 'Product - A product was updated'
            message = get_template('mail_template.html').render({
                'changes': pts,
                'first_message': ' made some changes in a product containing ID ' + str(obj.id),
                'user': self.request.user.username
            })
            serializer.validated_data['updated_by'] = self.request.user
            serializer.save()
            to_list = []
            for user in User.objects.filter(is_staff=True):
                to_list.append(user.email)
            from_email = settings.EMAIL_HOST_USER
            mail = EmailMessage(subject=mail_subject, body=message, from_email=from_email, to=to_list,
                                reply_to=[from_email])
            mail.content_subtype = 'html'
            mail.send()
            return Response(serializer.data)
        else:
            return Response({'detail': 'You are not authorized to update product'}, status.HTTP_400_BAD_REQUEST)


class TrackAPIView(viewsets.GenericViewSet,
                   mixins.ListModelMixin,
                   mixins.RetrieveModelMixin):
    serializer_class = TrackerSerializer
    queryset = ProductQueryTrack.objects.all()


class TrackByProductAPIView(APIView):
    serializer_class = TrackerSerializer
    queryset = ProductQueryTrack.objects.all()

    def get(self, request, id):
        return Response(self.serializer_class(self.queryset.filter(product_id=id), many=True).data)


def indexView(request):
    return redirect('schema-swagger-ui')
