# Catalog_v2

A product should have basic info such as sku, name, price and brand.

## Getting started

Open the PowerShell or cmd there and install the dependencies using the following command:

**pip install -r requirements.txt**

Now create the migrations using the following command (run the command one by one).

**python or python3 manage.py migrate**

**python or python3 manage.py makemigrations product**

**python or python3 manage.py migrate**

Now start the development server using the following command

**python or python3 manage.py runserver**

The development server will be run in localhost or 127.0.0.1 opening a port at 8000. So you can visit the project at http://127.0.0.1:8000

For API documentation, please visit http://127.0.0.1:8000/doc

You can create a superuser using the following command:

**python or python3 manage.py createsuperuser**


